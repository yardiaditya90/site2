import React, { useState } from 'react'
import '../styles/MainPage.css'
import { Fade } from "react-awesome-reveal";
import axios from 'axios';
export default function ContactUs() {
    const [css, changeCSS] = useState('')
    const [name, changeName] = useState('');
    const [job, changeJob] = useState('');
    const [company, changeCompany] = useState('');
    const [website, changeWebsite] = useState('');
    const [email, changeEmail] = useState('');
    const [phone, changePhone] = useState('');
    const [briefDescription, changeBriefDescription] = useState('');
    const [option, changeOption] = useState('1');

    const arrow = '→'
    const handleSelect = (e) => {
        e.target.parentNode.parentNode.className = 'element-selected'
    }

    const handleBlur = (e) => {
        e.target.parentNode.parentNode.className = 'element'
    }
    const handleSubmit = async () => {
        const requestData = {
            'option': option,
            'name': name,
            'job': job,
            'company': company,
            'website': website,
            'email': email,
            'phone': phone,
            'briefDescription': briefDescription,
            'option': option
        }
        try {
            // https://site2-8wfd.onrender.co2m
            // http://localhost:800a0
            const response = await axios.post('https://site2-8wfd.onrender.com/api/contactUs', requestData)
            if (response.data.status == true) {
                alert(response.data.message)
                changeName('')
                changeCompany('')
                changeEmail('')
                changeJob('')
                changePhone('')
                changeWebsite('')
                changeBriefDescription('')
            }
            else {
                alert("Something else also went wrong")
            }
        }
        catch (e) {
            alert('Something went wrong')
        }

    }
    return (
        <div style={{ backgroundColor: 'black' }}>
            <Fade>
                {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
                <div className='contact-us' style={{ margin: 'auto', width: '100%' }}>
                    <div className='top'>
                        {/* <h1 style={{fontSize:'200px'}}>CONTACT</h1> */}
                        <img src={require('../images/contactUs.jpg')} style={{ maxWidth: '90%', paddingTop: '40px' }} />
                        <div className='hidden-text' style={{ width: '100%', height: '0.5px', backgroundColor: 'white', zIndex: '-1', marginTop: '58px' }}></div>
                    </div>


                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                        {/* <div className='hidden-text c-left' style={{ maxWidth: '50%' ,paddingTop:'58px',width:'47.7%'}}> */}
                        <div className='hidden-text c-left contact-us-2'>
                            <p className='about-font-header'>Whatever you’ve got going on, we’re keen to here about it.</p>
                        </div>

                        <div className='hidden-text-visible-stuff c-right contact-box-outline contact-us-2 contact-us-2-r'>
                            <div>
                                <p style={{ marginTop: '0px', marginBottom: '5px', marginLeft: '20px' }} className='heading-text'>How can we help?</p>
                            </div>

                            <div className='contact-box' style={{ marginLeft: '15px', marginTop: '12px' }}>
                                <button className={`contact-btn${(option == '1' ? '-selected' : '')}`} onClick={() => (changeOption('1'))}>I NEED BANGING DESIGNS</button>
                                <button className={`contact-btn${(option == '2' ? '-selected' : '')}`} onClick={() => (changeOption('2'))}>I WANT TO WORK HERE</button>
                                <button className={`contact-btn${(option == '3' ? '-selected' : '')}`} onClick={() => (changeOption('3'))}>SOMETHING ELSE</button>
                            </div>

                            <div className='form'>
                                <div className='element'>
                                    <div className='form-input'>
                                        <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={name} onChange={(e) => (changeName(e.target.value))} />
                                        {/* changeCSS('-selected') */}
                                        <label>NAME</label> </div>
                                    <div className='divide-bar'></div>

                                </div>
                                <div className='element'>
                                    <div className='form-input'>
                                        <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={job} onChange={(e) => (changeJob(e.target.value))} />
                                        <label>JOB ROLE</label>
                                    </div>
                                    <div className='divide-bar'></div>

                                </div>
                                <div className='element'>
                                    <div className='form-input'>
                                        <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={company} onChange={(e) => (changeCompany(e.target.value))} />
                                        <label>COMPANY</label>
                                    </div>
                                    <div className='divide-bar'></div>

                                </div>
                                <div className='element'>
                                    <div className='form-input'>
                                        <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={website} onChange={(e) => (changeWebsite(e.target.value))} />
                                        <label>WEBSITE</label>
                                    </div>
                                    <div className='divide-bar'></div>

                                </div>
                                <div className='element'>
                                    <div className='form-input'>
                                        <input type='email' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={email} onChange={(e) => (changeEmail(e.target.value))} />
                                        <label>EMAIL</label>
                                    </div>
                                    <div className='divide-bar'></div>

                                </div>
                                <div className='element'>
                                    <div className='form-input'>
                                        <input type='number' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={phone} onChange={(e) => (changePhone(e.target.value))} />
                                        <label>PHONE</label>
                                    </div>
                                    <div className='divide-bar'></div>
                                </div>

                                {/* <div className='element'>
                                    <div className='form-input'>
                                        <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={briefDescription} onChange={(e) => (changeBriefDescription(e.target.value))} />
                                        <label>BRIEF DESCRIPTION</label>
                                    </div>
                                    <div className='divide-bar'></div>
                                </div> */}
                                <div className='element'>
                                    <div className='form-input'>
                                        <textarea rows="5" onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={briefDescription} onChange={(e) => (changeBriefDescription(e.target.value))}></textarea>
                                        <label>BRIEF DESCRIPTION</label>
                                    </div>
                                    <div className='divide-bar'></div>
                                </div>

                            </div>
                            {/* <div style={{ display: 'flex', padding: '8px', paddingTop: '25px', paddingBottom: '25px', margin: 'auto' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                    <div style={{ width: '95%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
                <div>
                    <p style={{ fontSize: '20px', marginTop: '0px', marginBottom: '5px' }}>A team of 200 people, it's really cool</p>
                </div>
                <div style={{ width: '95%', height: '.5px', backgroundColor: 'gray', margin: 'auto', marginTop: '30px', marginBottom: '20px' }}></div> */}
                            <div style={{ paddingTop: '25px', display: 'flex', paddingInline: '15px', justifyContent: 'space-between', paddingLeft: '0px' }}>
                                <button className='btn-white-big' onClick={handleSubmit} style={{ marginLeft: '0px', padding: '18px', width: '150px' }}>SEND {arrow}</button>
                                <div style={{ display: 'flex', flexDirection: 'column' }}>
                                    <p style={{ display: 'flex', color: 'gray', fontSize: '12px', margin: 'auto', marginBottom: '5px' }}>OR EMAIL US</p>
                                    <button style={{ backgroundColor: 'rgb(0,0,0,0)', color: 'gray', border: '1px solid gray', padding: '6px', fontSize: '10x', margin: 'auto' }}>HEY@gmail.com {arrow}</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }} className='gradient'></div>
            </Fade>
        </div>
    )
}
