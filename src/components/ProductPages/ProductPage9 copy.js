import React from 'react'
import '../../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage9() {
  return (<div>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>


          <div style={{paddingLeft:'46px'}}>
            <h1 style={{ color: 'white', textAlign: 'left' ,fontSize:'80px'}} className='heading-text'>
              Forex Trading Platform</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>Amazing Product</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        <img src={require('../../images/products/ftp1.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              Forex trading has always been a lucrative business, but it can also be quite challenging to navigate the complexities of the market. Fortunately, Anantkaal has come up with a solution for Indian traders and brokers with its latest web app designed specifically for forex trading.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Challenges</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              With this app, users can place orders for buying and selling currencies, keep up-to-date with the latest news in the forex market, and access a host of other features designed to make forex trading easier and more profitable. Anantkaal has integrated the latest security measures to ensure that users' personal and financial information is always safe and secure.
              </p>
            </div>
          </div>
        </div>
        <img src={require('../../images/products/ftp2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              One of the main advantages of our forex trading app is its simplicity. Even inexperienced traders can use the app with ease, thanks to its intuitive user interface. In addition, the app provides users with real-time updates on the forex market, helping them make informed decisions about when to buy and sell currencies. It provides access to a wide range of financial instruments, including major and minor currency pairs, indices, commodities, and more


This forex trading app has been a game-changer for Indian retail and enterprise traders and brokers. The app's user-friendly interface and the speed of execution has made it a favorite among traders. With the forex market expected to grow at a CAGR of 6.5% between 2021-2026, the demand for reliable and efficient trading platforms is only going to increase.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Our Solution</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                
              The app is ideal for both retail and enterprise traders and brokers. Retail traders can use the app to keep up-to-date with the latest market news and place orders quickly and easily. Meanwhile, enterprise traders and brokers can use the app to manage their clients' accounts, monitor their trades, and provide them with the latest market insights.


                  </p>
            </div>
          </div>
        </div>

        {/* <img src={require('../../images/productImage.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' /> */}

      </Fade>
    </div></div>
  )
}
