import React from 'react'
import '../../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage2() {
  return (<div>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>


          <div style={{ paddingLeft: '46px' }}>
            <h1 style={{ color: 'white', textAlign: 'left', fontSize: '80px' }} className='heading-text'>Diamond Cutter</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>World's number 1 Diamond Cutter, just buy it, its great!</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        <img src={require('../../images/products/P_21.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                As the diamond industry continues to evolve, the need for automation is increasingly apparent. One such technology that has proven to be a game-changer in the field of diamond polishing is the fully automatic diamond polishing machine, made by yours truly.
              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Challenges</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                Manual diamond polishing has many problems such as :
                Lack of precision and consistency
                Lower quality product and a lower price for the diamond
                High cost of labordifficult to scale up production
                Risk of injury to the workers

                Overall, manual diamond polishing is a slow, labor-intensive and costly process that can lead to lower quality diamonds, higher production costs, and safety and environmental concerns
              </p>
            </div>
          </div>
        </div>
        <img src={require('../../images/products/P_22.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                One of the key benefits of the fully automatic diamond polishing machine is its ability to significantly increase production capabilities. With its advanced capabilities, clients are able to produce up to twice as many diamonds as they could with traditional methods. This increased production can lead to significant cost savings and increased revenue for the client.
                In addition to increasing production capabilities, the fully automatic diamond polishing machine also has the potential to significantly lower production costs. By reducing the need for manual labor and increasing the efficiency of the polishing process, the machine can lower costs by as much as 21%. This makes it an extremely cost-effective solution for diamond polishing.
                The fully automatic diamond polishing machine is also highly robust and reliable. Its advanced technology and precision engineering ensure that it is able to withstand the rigors of daily use in a diamond polishing facility.
                This technology is the future of diamond polishing and it is an efficient and cost-effective solution to the diamond polishing industry.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Our Solution</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                This state-of-the-art machine is capable of polishing full pavilion facets and 16/8 crown diamonds with unparalleled symmetry. Its advanced technology and precision engineering allow for a level of accuracy and consistency that was previously unattainable through manual methods.
                One of the key benefits of the fully automatic diamond polishing machine is its ability to significantly increase production capabilities. With its advanced capabilities, clients are able to produce up to twice as many diamonds as they could with traditional methods. This increased production can lead to significant cost savings and increased revenue for the client.
                In addition to increasing production capabilities, the fully automatic diamond polishing machine also has the potential to significantly lower production costs. By reducing the need for manual labor and increasing the efficiency of the polishing process, the machine can lower costs by as much as 21%. This makes it an extremely cost-effective solution for diamond polishing.
                The fully automatic diamond polishing machine is also highly robust and reliable. Its advanced technology and precision engineering ensure that it is able to withstand the rigors of daily use in a diamond polishing facility.
                This technology is the future of diamond polishing and it is an efficient and cost-effective solution to the diamond polishing industry.              </p>
            </div>
          </div>
        </div>

        {/* <img src={require('../../images/productImage.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' /> */}

      </Fade>
    </div></div>
  )
}
