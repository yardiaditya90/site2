import React from 'react';
import '../../styles/MainPage.css';
import { Fade } from 'react-awesome-reveal';

export default function ProductPage2() {
  return (
    <div>
      <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
        <div className='header-responsive'>
          <Fade>
            <div>
              <h1 style={{ color: 'white', textAlign: 'left' }} className='heading-text'>Predictive Maintenance</h1>
              <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>Check the exact condition of your machinery in real-time with our Predictive Maintenance Solutions.</h2>
            </div>
            <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
          </Fade>
        </div>
        <Fade>
          <img src={require('../../images/products/pm1.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
          <div>
            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              {/* <div className='res-width' style={{ width: '100%' }}>
                <div style={{ display: 'flex', flexDirection: 'row' }}>
                  <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
                  <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
                </div>
                <p style={{ color: 'gray', textAlign: 'left' }}>
                Tired of unexpected machine breakdowns halting your production line?
Don't wait for a machinery breakdown to happen.

Check the exact condition of your machinery in real-time with our Predictive Maintenance Solutions.
With Amazon's monitron sensors installed, temperature and vibrations are tracked and the recordings are uploaded to the AWS cloud.
                  </p>
              </div> */}
            </div>

            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              <div className='res-width' style={{ width: '100%' }}>
                <div>
                  <h2 className='responsive-h2'>Features</h2>
                </div>
              </div>
              <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
                <div>
                  <div style={{ display: 'flex', padding: '8px' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                    <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                  </div>
                </div>
                <p style={{ fontSize: '18px' }}>
                Our app extracts and interprets raw data from AWS servers to provide an intuitive and simple representation of the machinery's status. The app's easy-to-read charts, graphs, and alarms bring attention to the machines' health and predict upcoming maintenance needs.

                </p>
              </div>
            </div>

            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              <div className='res-width' style={{ width: '100%' }}>
                <div>
                  <h2 className='responsive-h2'>The PlayPlayer Community</h2>
                </div>
              </div>
              <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width'>
                <div>
                  <div style={{ display: 'flex', padding: '8px' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                    <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                  </div>
                </div>
                <p style={{ fontSize: '18px' }}>
                Where players post photos of their scores, matches, and memories. It's what binds the players to keep coming back for more matches. It can also be used to discuss experiences at different clubs and address friction promptly.
                </p>
              </div>
            </div>
            <img src={require('../../images/products/pm2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />

            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              <div className='res-width' style={{ width: '100%' }}>
                <div>
                  <h2 className='responsive-h2'>For Sports Club Owners</h2>
                </div>
              </div>
              <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
                <div>
                  <div style={{ display: 'flex', padding: '8px' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>03</p>
                    <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                  </div>
                </div>
                <p style={{ fontSize: '18px' }}>
                Our app extracts and interprets raw data from AWS servers to provide an intuitive and simple representation of the machinery's status. The app's easy-to-read charts, graphs, and alarms bring attention to the machines' health and predict upcoming maintenance needs.

                </p>
              </div>
            </div>

            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              <div className='res-width' style={{ width: '100%' }}>
                <div>
                  <h2 className='responsive-h2'>Best Features</h2>
                </div>
              </div>
              <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width'>
                <div>
                  <div style={{ display: 'flex', padding: '8px' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>04</p>
                    <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                  </div>
                </div>
                <p>
                The greatest benefit is that it reduces the likelihood of unscheduled maintenance that could lead to costly downtime—no working hours wasted!

Stay ahead of the game with our app for monitoring machinery status
Contact us now to customise your own predictive maintenance monitor.
                </p>
              </div>
            </div>
          </div>
        </Fade>
      </div>
    </div>
  );
}
