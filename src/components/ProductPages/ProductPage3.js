import React from 'react'
import '../../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage3() {
  return (<div>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>


          <div style={{ paddingLeft: '46px' }}>
            <h1 style={{ color: 'white', textAlign: 'left', fontSize: '80px' }} className='heading-text'>Smart EV Charging Outlet</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>Are you tired of other people using your EV charging point? Locking up your EV plug point is a ‘jugaad’ solution, So we created our smart plug point that can only be unlocked with a single tap.</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        <img src={require('../../images/products/ev.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                Locking up your EV plug point is a ‘jugaad’ solution, So we created our smart plug point that can only be unlocked with a single tap.

                Our smart plug socket can only be unlocked with the given RFID card or keychain, which ensures that no one else can use your electricity. It starts charging with a tap of your card on the scanner on the plug point.
                It will disconnect once the charger is unplugged, and will need to be tapped again to restart charging. So, nobody can remove your charger and plug in their own.

                It cuts off charging once the battery is fully charged, saving you electricity and reducing the risk of overcharging. We provide one RFID card and one keychain per plug point by default, but more can be ordered as needed.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Its really helpful</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                For larger housing societies or parking areas with multiple plug points, we also offer customized ports that come with two or three common card scanners and 12-20 sockets. This allows accurate measurement of electricity consumption by each user and prevents clutter on the parking lot electricity outlet.

                Don’t let the inconvenience and security risks of using a public charging plug point deter you from driving an EV. With our smart plug point, you can charge your EV with peace of mind and convenience, knowing that only you can access your electricity.              </p>
            </div>
          </div>
        </div>
        <img src={require('../../images/products/ev2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />


        {/* <img src={require('../../images/products/ev2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' /> */}

      </Fade>
    </div></div>
  )
}
