import React from 'react';
import '../../styles/MainPage.css';
import { Fade } from 'react-awesome-reveal';

export default function ProductPage8() {
  return (
    <div>
      <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
        <div className='header-responsive'>
          <Fade>
            <div>
              <h1 style={{ color: 'white', textAlign: 'left' }} className='heading-text'>Real Estate Marketplace</h1>
              <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>The solution you've been looking for</h2>
            </div>
            {/* Start: */}
            <div>
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
                <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
              </div>
              <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product</p>
              <div className='button-box'>
                <button>BRAND</button>
                <button>PRODUCT DESIGN</button>
                <button>PROTOTYPE</button>
              </div>
            </div>
            {/* End */}
          </Fade>
        </div>
        <Fade>
          <img src={require('../../images/products/rem1.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
          <div>
            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              <div className='res-width' style={{ width: '100%' }}>
                <h2 className='responsive-h2'>Tech Stack</h2>
              </div>
              <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
                <div>
                  <div style={{ display: 'flex', padding: '8px' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                    <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                  </div>
                  <p>Tired of browsing endless property listings and wasting time on viewings? Our latest project for a real estate client in Singapore might be the solution you've been looking for! At Anantkaal, we have developed a cutting-edge web app that makes property hunting a breeze for both agents and users.</p>
                </div>
                <ul style={{ fontSize: '18px', listStyleType: 'disc', paddingLeft: '20px' }}>
                  <li>Flutter for Admin Panel and Mobile App</li>
                  <li>HTML / CSS / JS for Website</li>
                  <li>Laravel for API</li>
                  <li>Figma for Design</li>
                </ul>
              </div>
            </div>
            <img src={require('../../images/products/rem2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />

            <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
              <div className='res-width' style={{ width: '100%' }}>
                <h2 className='responsive-h2'>Features</h2>
              </div>
              <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width'>
                <div>
                  <div style={{ display: 'flex', padding: '8px' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                    <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                  </div>
                </div>
                <p style={{ fontSize: '18px' }}>
                  The platform offers a user-friendly interface where agents can easily list properties with images and amenities, while users can browse and filter through a variety of properties across countries. With the ability to book, rent, or purchase, our app streamlines the entire process of property transactions.
                </p>
              </div>
            </div>
            
            {/* Add more sections here as needed */}
            
          </div>
        </Fade>
      </div>
    </div>
  );
}
