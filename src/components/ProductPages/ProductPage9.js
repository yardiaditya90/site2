import React from 'react'
import '../../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage9() {
  return (<div>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>


          <div style={{paddingLeft:'46px'}}>
            <h1 style={{ color: 'white', textAlign: 'left' ,fontSize:'80px'}} className='heading-text'>
            Office Devices Automation</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>While solving for enterprises, we found a way to level up with Smart Offices.</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        <img src={require('../../images/products/oda1.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              After fitting in the smart switchboard, Our intuitive app lets you control everything from lights, fans, kitchen appliances & air conditioning. Centralized temperature control and scheduling of fixtures will ensure that you can keep your office at the right temperature, no matter what time of the day it is.          </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Challenges</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              Say goodbye to cluttered switchboards and hello to sleek, modern touchscreen switchboards. Our app has been designed with defined user flows, so you or the office operator don't have to navigate through a maze of options to flip a switch.
              </p>
            </div>
          </div>
        </div>
        <img src={require('../../images/products/oda2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              The Atom Office Automation System is not just about convenience. It's about boosting productivity and saving money. Minimise wasting electricity by scheduling lights and appliances to turn off and on according to working hours.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Our Solution</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                
              ThWorking in a futuristic haven is known to have a positive effect on the workforce’s output. 
Get in touch with us and see what our Office Automation System can do for your business!


                  </p>
            </div>
          </div>
        </div>

        {/* <img src={require('../../images/productImage.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' /> */}

      </Fade>
    </div></div>
  )
}
