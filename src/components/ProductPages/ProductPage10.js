import React from 'react'
import '../../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage10() {
  return (<div>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>


          <div style={{paddingLeft:'46px'}}>
            <h1 style={{ color: 'white', textAlign: 'left' ,fontSize:'80px'}} className='heading-text'>Commodities Trading Platform</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>Tired of not getting the best prices? We built the Quicksteel App just for that!</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        <img src={require('../../images/products/ctp1.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              As a trader in industrial commodities, you need access to real-time pricing information that's reliable, accurate, and up-to-date. With daily prices for essential commodities such as steel, coal, and iron, Quicksteel is your go-to destination for real-time pricing information.
It isn't just about viewing prices. It's a comprehensive trading platform that allows buyers to place orders and enquiries directly with sellers.


              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Challenges</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              One of the standout features of Quicksteel is its sleek, user-friendly interface that makes it easy to place orders, enquiries, and contact sellers. With just a few taps, you can buy or sell commodities, get in touch with sellers, and stay on top of your trading game.
              </p>
            </div>
          </div>
        </div>
        <img src={require('../../images/products/ctp2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              It also includes a sentiment analysis feature that helps you build a community around your trades. This feature analyzes market sentiment and allows you to connect with like-minded traders and share your experiences and insights.  Plus, with a subscription-based pricing model, you can access the latest and most detailed information at your fingertips.             
                 </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Our Solution</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              From ideation to launch, Mandi was developed within two months, including the design and testing. The Anantkaal's team of experienced developers and designers worked tirelessly to ensure a seamless user experience. The greatest challenge was to choose a server within the budget that could support large amounts of data, but we overcame that after 4 trials!
                </p>
            </div>
          </div>
        </div>

        {/* <img src={require('../../images/productImage.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' /> */}

      </Fade>
    </div></div>
  )
}
