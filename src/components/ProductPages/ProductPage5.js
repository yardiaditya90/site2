import React from 'react'
import '../../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage5() {
  return (<div>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', width: 'fit-content', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>


          <div style={{paddingLeft:'46px'}}>
            <h1 style={{ color: 'white', textAlign: 'left' ,fontSize:'80px'}} className='heading-text'>Infant Radiant Warmer</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>Saving Lives, One Baby at a Time - A Baby Warmer for Premature Babies</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>


              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>

            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        <img src={require('../../images/products/irw2.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              Premature babies require an environment that mimics the mother's womb to survive and thrive. The Baby Warmer is an infant radiant warmer developed by us back in 2019. It is used in hospitals under the supervision of medical professionals, ensuring a controlled and sterile environment for premature babies.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Designs</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              Our Baby Warmer sports a custom-designed PCB (printed circuit board) and controller, which has been equipped with multiple sensors for temperature, sterility, humidity, and UV light. 
The temperature sensors are highly accurate to 0.01-degree variations, and a custom algorithm has been developed to provide precise temperature control for the neonatal unit. This ensures that the environment is optimal for the baby's growth and development. 
It also supports plugins for ECG Sensors, for monitoring vitals like SPO2.
The device also includes a feedback loop that allows healthcare providers to monitor the baby's vitals remotely and make adjustments as needed.              </p>
            </div>
          </div>
        </div>
        <img src={require('../../images/products/irw3.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              One of the most critical features of the Baby Warmer device is the B-type ultraviolet light (UVB). Premature babies often have low levels of vitamin D, which is important for bone growth and immune system function. UVB light helps to convert a substance in the skin called 7-dehydrocholesterol into vitamin D3.
This process is especially important in premature babies who may not have received enough vitamin D from their mothers during pregnancy. Additionally, B-type UV light is also used to create a sterile environment in the neonatal care unit to reduce the risk of infection for these vulnerable infants.              </p>
            </div>
          </div>

          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>


              </div>
              <h2 className='responsive-h2'>Results</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
              The Baby Warmer has been tested extensively and has shown remarkable results. Babies that have been placed in the Baby Warmer have experienced more stable growth than those in traditional incubators. The device has also been shown to reduce the risk of infections and other complications associated with premature birth.              </p>
            </div>
          </div>
        </div>
        
      </Fade>
    </div></div>
  )
}
