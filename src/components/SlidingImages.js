import React from 'react'
import '../styles/MainPage.css'

export default function SlidingImages() {
    return (
        <div>
            <div style={{ display: 'flex', flexDirection: 'row',overflow:'hidden'}}>
                <div className='images-container slider'>
                    <img src={require('../images/logo/logo1.jpg')} style={{ width: '100px' }} />
                    <img src={require('../images/logo/logo2.jpg')} style={{ width: '100px' }} />
                </div>
                <div className='images-container slider'>
                    <img src={require('../images/logo/logo1.jpg')} style={{ width: '100px' }} />
                    <img src={require('../images/logo/logo2.jpg')} style={{ width: '100px' }} />
                </div>
                {/* <div className='images-container slider'>
                    <img src={require('../images/logo/logo1.jpg')} style={{ width: '100px' }} />
                    <img src={require('../images/logo/logo2.jpg')} style={{ width: '100px' }} />
                </div> */}
            </div>
        </div>
    )
}
