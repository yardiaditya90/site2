const data=[
    {
        "name":"Product 1",
        "price":"350",
        "category":"C1",
        "profilePic":"../images/image1.jpg",
        "image":["../images/image1.jpg","../images/image2.jpg","../images/image3.jpg"],
        "description":"Amazing product, really cool!",
        "company":"Company 1",
        "html":['Its great','Its really cool','Its epic','Its amazing']
    }
]
export default data;