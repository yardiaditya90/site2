import React from 'react'
import '../styles/MainPage.css'
import '../styles/css2.css'
import AboutPageCard from './AboutPageCard';
import SlidingImagesPeople from './SlidingImagesPeople';
import { Fade } from 'react-awesome-reveal';
export default function AboutPage() {
    const arrow = `->`;
    return (
        <div style={{ backgroundColor: 'black' }} >
            {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
            <Fade>
                <div className='header' style={{ paddingBottom: '58px' }}>
                    <div className='top'>
                        {/* <h1>ABOUT</h1> */}

                        {/* <h1 style={{fontSize:'200px'}}>ABOUT</h1> */}
                        <img src={require('../images/about.jpg')} style={{ maxWidth: '90%', paddingTop: '40px', paddingBottom: '35px' }} />
                        {/* <div className='hidden-text' style={{ width: '100%', height: '0.5px', backgroundColor: 'white', zIndex: '-1', marginTop: '58px' }}></div> */}

                    </div>


                    <div className='header-responsive'>
                        <div>
                            <div style={{ display: 'flex', padding: '8px' }}>
                                <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>Work we do</p>
                                <div style={{ width: '75%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                            </div>
                            <div>
                                <p className='about-font-header' >An extraordinary design studio.</p>
                            </div>
                        </div>


                        <div>
                            <div style={{ display: 'flex', padding: '8px' }}>
                                <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                                <div style={{ width: '75%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                            </div>
                            <div>
                                <p style={{ fontSize: '12px', marginTop: '0px', marginBottom: '5px', color: 'gray' }}>TLDR; We’re a team of designers who create products and brands that bang.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }} className='gradient'></div>

                <div style={{ backgroundColor: 'white', padding: '15px' }}>
                    <SlidingImagesPeople />
                    <div className='header-responsive about-info-box'>
                        {/* <SlidingImagesPeople /> */}
                        <div className='about-info-left'>
                            <div style={{ display: 'flex' ,justifyContent:'flex-start'}}>
                                <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                                <div style={{ width: '97%', height: '1px', backgroundColor: 'gray',marginLeft:'10px', margin: 'auto' }}></div>
                            </div>
                            <div style={{marginTop:'25px'}}>
                                <p style={{ color: 'black' ,marginLeft:'0px'}} className='desc'>We’re always looking for the right people to join our team. If you feel we’re a match for what you’re about, we’d love hear from you.</p>
                            </div>
                        </div>

                        <div className='about-info-right'>
                            <div style={{ display: 'flex' }}>
                                <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>CARRERS</p>
                                <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>

                            </div>
                            <div>
                                <p style={{ color: 'gray', fontSize: '14px' ,marginTop:'25px',marginLeft:'0px'}}>We hire based on people-fit first, design second, and bureaucracy can do one.</p>
                               <div style={{display:'flex',flexDirection:'row'}}> <button className='gray-outline-button' style={{ marginLeft: '0px', marginBottom: '20px' }}>OPEN ROLES</button></div>
                            </div>
                        </div>
                    </div>

                    <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', paddingTop: '15px', }}>
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' />
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' button='WORK' />
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' />
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' />
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' />
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' />
                        <AboutPageCard title='12 People' text='Teeth cut in all things design' />
                    </div>
                </div>
                <div style={{ backgroundColor: 'black', display: 'flex', flexDirection: 'column', justifyContent: 'flex-start', paddingLeft: '20px', paddingRight: '15px', paddingBottom: '25px' }}>
                    <h2 style={{ color: 'white', display: 'flex' ,marginLeft:'0px',marginTop:'20px',marginBottom:'15px'}} className='heading-text-2'>The Company</h2>
                    <p style={{ color: 'white', display: 'flex', margin: '0px' }}>
                        We pride ourselves on a well defined, tried & tested, no-bull approach to designing award winning products & brands.
                        Our designers have their teeth cut in digital design, you are the specialist in your field, together, we create products that disrupt and brands that captivate.
                        We do away with the traditional agency way of working, you don’t outsource design, we become in-house designers.
                        We act as your tried & tested internal design team, capable of crafting everything an award winning product needs, delivering unmatched attention to detail, and holistic design from a service level right down to the products in your users hands.
                    </p>
                    <img src={require('../images/building.jpg')} style={{ width: '100%', maxWidth: '100%', marginTop: '13px' }} />
                    {/* <h2 style={{ color: 'white', display: 'flex' }}>Workplace</h2>
                    <p style={{ color: 'white', display: 'flex', margin: '0px' }}>
                        We work here, it's really cool, epic one!
                    </p> */}
                </div>
            </Fade>
        </div>
    )
}
