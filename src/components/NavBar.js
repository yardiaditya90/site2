import React, { useEffect, useState } from 'react'
import '../styles/MainPage.css'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
export default function NavBar() {
    const navigate = useNavigate()
    const arrow = `→`
    const [dropDown, changeDropDown] = useState(null)

    const handleDropDown = () => {
        if (dropDown == null) {
            changeDropDown(
                <>
                    <div className='dropdown-list'>
                        <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}} to='/'>Home</Link> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Work</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Services</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}}  to='/about'>About</Link> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Blog</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Careers</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}}  to='/contactUs'>Contact</Link> <h2>{arrow}</h2>
                        </div>
                        {/* <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}}  to='/AddProduct'>Add Product</Link> <h2>{arrow}</h2>
                        </div> */}
                        <div><h1>Featured Products</h1></div>
                    </div></>
            )
        }
        else {
            changeDropDown(null)
        }
    }
    return (<>
        <div className='navBar' style={{paddingRight:'0px',paddingLeft:'0px'}}>
            <div className='logo' style={{ cursor: 'pointer' ,borderRight:'1px solid gray',height:'100%',paddingInline:'20px',display:'flex'}} onClick={() => (navigate('/'))}>
                <h1 style={{margin:'auto',fontFamily:'Cy Grotesk Grand Dark'}}>B</h1>
                {/* <div style={{width:'20px',height:'',backgroundColor:'white'}}></div> */}
            </div>

            <div className='dropdown'>
                <div className='nav-r hidden-text'>
                <Link to='/' className='link'>WORK</Link>
                <Link to='/about' className='link'>ABOUT</Link>
                </div>
                <div className='dropDownButton' onClick={handleDropDown} style={{margin:'0px',margin:'auto',marginInline:'17.5px'}}>
                    <div style={{ display: 'none' }}></div>
                    <div className='stripes'></div>
                    <div className='stripes'></div>
                    <div className='stripes'></div>
                </div>
            </div>
        </div>
        <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', zIndex: '-1' }}></div>
        {dropDown}
    </>
    )
}
