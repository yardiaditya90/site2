import React from 'react'
import '../styles/Products.css'

export default function AboutPageCard(props) {
  return (
    <div className='about-page-card'>
      <div style={{display:'flex',flexDirection:'row',justifyContent:'flex-end',padding:'15px',paddingTop:'15px'}}>
        {props.button?(<button className='gray-outline-button' style={{ marginRight: '0px', marginBottom: '20px' }}>{props.button}</button>):''}
      </div>
      <div style={{display:'flex',flexDirection:'column',paddingBottom:'15px'}}>
      <h3>{props.title}</h3>
      <p style={{display:'flex',color:'gray',margin:'auto',marginBottom:'0px',textAlign:'center',fontSize:'15px'}}>{props.text}</p>
      <div style={{ width: '75%', height: '1px', backgroundColor: 'gray', margin: 'auto' ,marginTop:'15px',marginBottom:'5px'}}></div>
      </div>
    </div>
  )
}
