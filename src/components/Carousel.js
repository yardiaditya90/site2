import React, { useState, useEffect } from 'react';
import '../styles/Carousel.css'; // Import your CSS file for styling

const randomImages = [
    'https://via.placeholder.com/500x300', // Example image URL 1
    'https://via.placeholder.com/500x300', // Example image URL 2
    'https://via.placeholder.com/500x300', // Example image URL 3
    // Add more image URLs as needed
];

export default function Carousel({ interval = 3000 }) {
    const [currentIndex, setCurrentIndex] = useState(0);

    useEffect(() => {
        const timer = setInterval(() => {
            setCurrentIndex((prevIndex) => (prevIndex + 1) % randomImages.length);
        }, interval);

        return () => {
            clearInterval(timer);
        };
    }, [interval]);

    return (
        <div className="carousel">
            <div className="image-container" style={{ transform: `translateX(-${currentIndex * 100}%)` }}>
                {randomImages.map((image, index) => (
                    <img key={index} src={image} alt={`image-${index}`} className="carousel-image" />
                ))}
            </div>
        </div>
    );
}
