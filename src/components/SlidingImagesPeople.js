import React from 'react'
import '../styles/MainPage.css'

export default function SlidingImagesPeople() {
    return (
        <div>
            <div style={{ display: 'flex', flexDirection: 'row',overflow:'hidden'}}>
                <div className='images-container slider'>
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                </div>
                <div className='images-container slider'>
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                </div>
                <div className='images-container slider'>
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                    <img src={require('../images/Rock.jpg')} style={{ width: '254px' ,borderRadius:'15px'}} />
                </div>
            </div>
        </div>
    )
}
