import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import '../App.css'
function PageTransition() {
  const { pathname } = useLocation();
  const [mode, setMode] = useState('close');

  useEffect(() => {
    const handleRouteChange = () => {
      setMode('open');
      setTimeout(() => {
        setMode('close');
      }, 1000); // Adjust the time according to your transition duration
    };

    // Subscribe to route changes
    window.addEventListener('popstate', handleRouteChange);

    // Cleanup
    return () => {
      window.removeEventListener('popstate', handleRouteChange);
    };
  }, []);

  return <div className={mode} style={{ backgroundColor: 'black', zIndex: '50', position: 'fixed', width: '100%', height: '100%' }}></div>;
}

export default PageTransition;
