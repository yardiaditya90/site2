import React, { useEffect } from 'react'
import SlidingImages from './SlidingImages'
import Header from './Header'
import ProductsArea from './ProductsArea'
import '../styles/MainPage.css'
import '../styles/Transition.css'
import { Fade } from "react-awesome-reveal";
import { Parallax } from 'react-scroll-parallax';
import { useNavigate } from 'react-router-dom'
export default function MainPage() {
  const navigate=useNavigate();
  useEffect(()=>{
    blockOpen()
  })
  const blockOpen=()=>{
    const blockerOpenElement = document.querySelector('.blocker-open');
    if (blockerOpenElement) {
      blockerOpenElement.className='blocker-open'
    }
  }
  const blockClose=()=>{
    const blockerOpenElement = document.querySelector('.blocker-open');
    if (blockerOpenElement) {
      blockerOpenElement.className='blocker-close'
    }
  }
  const arrow = `->`
  return (<>
  
    <div style={{width:'100%'}}>
    {/* <div className='blocker-open' onLoad={(e)=>{console.log("The class name is:",e.target.className)}}></div> */}
      <Fade>
        <Header />
        <SlidingImages />
        <div style={{backgroundColor:'black'}}>
        <br/>
        
        {/* <h1>sjkncosd</h1> */}
        <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }} className='gradient'></div></div>
        {/* <input/> */}
        <ProductsArea />
        <div style={{ maxWidth: '100%', padding: '15px', backgroundColor: 'black', justifyContent: 'space-around', backgroundColor: 'black' }}>
          <div style={{ width: 'auto', height: 'auto', overflow: 'hidden', borderRadius: '20px' }}>
            <Parallax speed={-35}>
              <img src={require('../images/Bike_01_b.jpg')} style={{ borderRadius: '0px', overflow: 'hidden' }} className='responsive-image' />
            </Parallax>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', marginTop: '12px', justifyContent: 'space-between', padding: '20px' }}>
            <p style={{ color: 'gray', margin: '0px', fontSize: '12px', margin: 'auto',
            //  marginLeft: '0px' 
             }}>Featured</p>
            {/* <div style={{display:'flex',flexDirection:'row'}}>  */}
            <button style={{ color: 'black', backgroundColor: 'white', borderRadius: '50px', margin: 'auto', border: '0px', padding: '12px', fontSize: '15px', margin: '0px 5px', width: '69px', height: 'fit-content', margin: 'auto' }}>
              Adax
            </button>
            {/* <button style={{color:'black',backgroundColor:'white',borderRadius:'50px',margin:'auto',border:'0px',padding:'12px',fontSize:'15px',margin:'0px 5px',width:'69px',height:'fit-content',margin:'auto'}}>
                {arrow}
              </button> */}
            {/* <p className='hidden-text' style={{color:'white'}}>
                skdnfllco oij oic ods coijsoc sodoic osj oij dosc jos.
              </p> */}
            {/* </div> */}

          </div>
        </div>
        
        
      </Fade>
    </div></>
  )
}
