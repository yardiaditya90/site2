import React from 'react'
import '../styles/Products.css'
import '../styles/MainPage.css'
import { useNavigate } from 'react-router-dom'
import { Fade } from "react-awesome-reveal";
export default function ProductsArea() {
    const navigate = useNavigate();
    return (
        <div className='product-display-area'>
            <div>
                <p style={{ color: 'white', textAlign: 'left', margin: '20px', fontWeight: 'bold', fontSize: '20px' }}>
                    {/* Everybody trusts this website cause it's the best ever!!! */}
                    {/* Everybody gangsta until they use this site!!! */}
                </p>
                <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '1' }}></div>
            </div>

            <div className='products'>
                {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '1' }}></div> */}
                <div className='main-page-text row-col-res' style={{ marginTop: '0px' }}>
                    <div style={{ display: 'flex', flexDirection: 'column', margin: 'auto', width: '100%', marginTop: '0px' }} className='res-width'>


                        <div style={{ flexDirection: 'column', margin: 'auto', marginLeft: '0px', marginBottom: '25px' ,paddingLeft:'35px'}} className='hidden-text'>
                            <h1 style={{ color: 'white', fontWeight: 'bold', fontSize: '50px', textAlign: 'left', display: 'flex' }}>Tried, tested & trusted by 80+ businesses to deliver world class design to their products & brands.</h1>
                            <div style={{ display: 'flex', flexDirection: 'row' }}><button className='btn-white-big'>See all work</button><div style={{ width: '75%', height: '1px', backgroundColor: 'gray', margin: 'auto' ,marginLeft:'0px'}}></div></div>
                        </div>
                        <div className='left'>
                            <div className='left-left' style={{ marginTop: '0px' }}>
                                <Fade>

                                    <div className='product-card'>
                                        <img src={require('../images/products/bw_01_b.jpg')} onClick={() => (navigate('/product5'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/rem1.jpg')} onClick={() => (navigate('/product8'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/P_03 (10).jpg')} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/other1.jpg')} />
                                    </div>
                                </Fade>
                            </div>
                            <div className='left-right' style={{ marginTop: '0px' }}>
                                <Fade>
                                    <div className='product-card'>
                                        <img src={require('../images/products/diamond_gif.gif')} onClick={() => (navigate('/product2'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/safe.jpg')} onClick={() => (navigate('/product6'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/ak.gif')} onClick={() => (navigate('/product11'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/other2.jpg')} />
                                    </div>
                                </Fade>
                            </div>
                        </div>
                    </div>

                    <div className='right' style={{ margin: '0px 10px' }}>

                        <div className='right-left' style={{ marginTop: '0px' }}>
                            <div className='product-card'>
                                <Fade>
                                    <div className='product-card'>
                                        <img src={require('../images/products/smv2.jpg')} onClick={() => (navigate('/product3'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/pm1.jpg')} onClick={() => (navigate('/product7'))} />
                                    </div>
                                    <div className='product-card'>
                                        <img src={require('../images/products/other3.jpg')} />
                                    </div>
                                </Fade>
                            </div>
                        </div>
                        <div className='right-right' style={{ marginTop: '0px' }}>
                            <Fade Boun>
                                <div className='product-card'>
                                    <img src={require('../images/products/flower.gif')} onClick={() => (navigate('/product4'))} />
                                </div>
                                <div className='product-card'>
                                    <img src={require('../images/products/hyd.jpg')} />
                                </div>
                                <div className='product-card'>
                                        <img src={require('../images/products/other4.jpg')} />
                                    </div>
                            </Fade>
                        </div>

                    </div>
                </div>
                {/* <div className='product-card'>
                    <img src={require('../images/image1.jpg')} />
                </div>

                <div className='product-card'>
                    <img src={require('../images/image2.jpg')} />
                </div>

                <div className='product-card'>
                    <img src={require('../images/image3.jpg')} />
                </div>

                <div className='product-card'>
                    <img src={require('../images/image2.jpg')} />
                </div>
                <div className='product-card'>
                    <img src={require('../images/image2.jpg')} />
                </div> */}
            </div>

        </div>

    )
}
