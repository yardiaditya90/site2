import React, { useEffect, useState } from 'react'
import '../styles/MainPage.css'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
export default function NewNavBar() {
    const navigate = useNavigate()
    const arrow = `→`
    const [dropDown, changeDropDown] = useState(null)

    const handleDropDown = () => {
        if (dropDown == null) {
            changeDropDown(
                <>
                    <div className='dropdown-list'>
                        <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}} to='/'>Home</Link> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Work</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Services</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}}  to='/about'>About</Link> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Blog</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <h2>Careers</h2> <h2>{arrow}</h2>
                        </div>
                        <div className='dropdown-list-items'>
                            <Link className='dropdown-link' onClick={()=>{changeDropDown(null)}}  to='/contactUs'>Contact</Link> <h2>{arrow}</h2>
                        </div>
                        <div><h1>Featured Products</h1></div>
                    </div></>
            )
        }
        else {
            changeDropDown(null)
        }
    }
    return (<>
        <div className='navBar' style={{backgroundColor:'white'}}>
            <div className='logo' style={{ cursor: 'pointer' }} onClick={() => (navigate('/'))}>
                <h1 style={{color:'black'}}>B</h1>
            </div>

            <div className='dropdown'>
                <div className='dropDownButton' onClick={handleDropDown}>
                    <div style={{ display: 'none' }}></div>
                    <div className='stripes' style={{backgroundColor:'black'}}></div>
                    <div className='stripes' style={{backgroundColor:'black'}}></div>
                    <div className='stripes' style={{backgroundColor:'black'}}></div>
                </div>
            </div>
        </div>
        <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div>
        {dropDown}
    </>
    )
}
