import React, { useEffect, useState } from 'react'
import '../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
import data from './data'
export default function ProductPage() {
  const [datas, setDatas] = useState(null)
  useEffect(() => {
    console.log(data)
    setDatas(data[0])
  })
  return (<div style={{width:'100%'}}>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px', paddingRight: '20px' }}>
      <div className='header-responsive'>
        <Fade>
          <div style={{paddingLeft:'46px'}}>
            <h1 style={{ color: 'white', textAlign: 'left' ,fontSize:'80px'}} className='heading-text'>{datas ? datas.name : ''}</h1>
            <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>{datas ? datas.description : ''}</h2>
          </div>

          <div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
              <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
            </div>
            <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
            </p>
            <div className='button-box'>
              <button>BRAND</button>
              <button>PRODUCT DESIGN</button>
              <button>PROTOTYPE</button>
            </div>
          </div>
        </Fade>
      </div>
      <Fade>
        {/* <img src={datas ? require(datas.image[0]) : ''} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' /> */}
        <img src={require('../images/image1.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />
        <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column', width: '50%' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                {datas ? datas.html[0] : ''}
              </p>
            </div>
          </div>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
              </div>
              <h2 className='responsive-h2'>What they faced</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column', width: '50%' }} className='res-width'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                {datas ? datas.html[1] : ''}
              </p>
            </div>
          </div>
        </div>
        {/* <img src={datas ? require(datas.image[1]) : ''} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />        */}
         <div className='product-info-box'>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
                <div style={{ display: 'flex', padding: '8px' }} className='hidden-text'>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                  <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <h2 className='responsive-h2'>About Them</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column', width: '50%' }} className='res-width res-border-left'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>01</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                {datas ? datas.html[2] : ''}
              </p>
            </div>
          </div>
          <div className='info-section header-responsive' style={{ justifyContent: 'space-between' }}>
            <div className='res-width' style={{ width: '100%' }}>
              <div>
              </div>
              <h2 className='responsive-h2'>What they faced</h2>
            </div>
            {/* <div style={{width:'2px',height:'200px',backgroundColor:'white'}}></div> */}
            <div style={{ display: 'flex', width: 'fit-content', flexDirection: 'column', width: '50%' }} className='res-width'>
              <div>
                <div style={{ display: 'flex', padding: '8px' }}>
                  <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>02</p>
                  <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
              </div>
              <p style={{ fontSize: '18px' }}>
                {datas ? datas.html[3] : ''}              
              </p>
            </div>
          </div>
        </div>
      </Fade>
    </div></div>
  )
}
