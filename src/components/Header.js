import React from 'react'
import '../styles/MainPage.css'
import '../styles/css2.css'
import { useNavigate } from 'react-router-dom'
export default function Header() {
    const arrow = `->`
    const navigate = useNavigate();
    return (<>
        <div className='header'>

            <div className='top'>
                <h1>ANANTKAAL</h1>
            </div>
            <div className='header-responsive about-info-box' style={{marginTop:'20px'}}>
                <div style={{ display: 'flex', flexDirection: 'column' }} className='heading-left'>
                    <div style={{ padding: '8px' }} className='hidden-text'>
                        <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                        <div style={{ width: '95%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                    </div>
                    <div className='mid' style={{ display: 'flex', flexDirection: 'column' }}>

                        <p className='heading-text-2'>Product design and manufacturing</p>
                    </div>
                </div>
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <div style={{ padding: '8px' }} className='hidden-text'>
                        <p style={{ fontSize: '12px', margin: '2px', color: 'gray',marginLeft:'14px' ,margin:'auto'}}>@2023</p>
                        <div style={{ width: '90%', height: '1px', backgroundColor: 'gray', margin: 'auto' ,marginInline:'5px'}}></div>
                        <div style={{display:'flex',flexDirection:'row'}}> <button style={{ marginLeft: '10px', marginBottom: '20px' ,backgroundColor:'black',color:'gray',fontSize:'10px',width:'90px',height:'30px',margin:'auto',border:'1px solid gray',padding:'6px'}}>SERVICE</button></div>
                    </div>
                    <div className='bottom' style={{ display: 'flex', flexDirection: 'column' ,paddingLeft:'0px'}}>
                        <p style={{margin:'0px',marginBottom:'20px',margin:'auto'}} className='hidden-text'>
                        Founded by designers, The Bang is a design studio housing unmatched talent producing their best work for the best companies.
                        </p>
                        <button onClick={()=>{navigate('/contactUs')}} style={{marginLeft:'0px',marginTop:'20px'}}>Work with us</button>
                    </div>
                </div>
            </div>
            {/* <p className='text-responsive-visibility'>WE BUILD BETTER.             AGILE. DYNAMIC. EFFICIENT.</p> */}
        </div>
        {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }} className='gradient'></div> */}
    </>
    )
}
