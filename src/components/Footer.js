import React from 'react'
import '../styles/MainPage.css'
import { Link, useNavigate } from 'react-router-dom'
export default function Footer() {
    const arrow = `->`
    const navigate=useNavigate();
    return (
        <div style={{ backgroundColor: 'white', display: 'flex', flexDirection: 'column', justifyContent: 'flex-start' ,paddingBottom:'8px'}}>
            
            <div className='header-responsive'>

            
            <div>

            
            <h2 style={{ textAlign: 'left', marginLeft: '20px' }} className='responsive-h1'>Ready? Chat with the team, and let's get going.</h2>
            <div className='footer'>
                <button onClick={()=>{navigate('/contactUs')}}>Work with us {arrow}</button>
            </div>
            </div>
            <div>

            
            {/* <div style={{ width: '80%', height: '1px', backgroundColor: 'black', margin: 'auto', marginTop: '15px', marginBottom: '5px' }}></div> */}
            <div className='footer-list header-responsive' style={{justifyContent:'space-evenly',padding:'20px'}}>
                <div style={{display:'flex',flexDirection:'column'}}>
                <div style={{ display: 'flex', flexDirection: 'row', marginTop: '10px' }}>
                    <h3>DESIGN</h3>
                    <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '5px', marginRight: '15px' }}></div>
                </div>

                <p>WORK</p>
                <p>BRANDING</p>
                <p>PRODUCT DESIGN</p>
                <p>DESIGN SYSTEM</p>
                <p>WEBSITE</p>
                <p>PITCHING & RAISING</p>
                </div>
                <div style={{display:'flex',flexDirection:'column'}}>
                <div style={{ display: 'flex', flexDirection: 'row', marginTop: '10px' }}>
                    <h3>THE BANG</h3>
                    <div style={{ width: '100%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '5px', marginRight: '15px' }}></div>
                </div>

                <p onClick={()=>{navigate('/about')}} style={{cursor:'pointer'}}>ABOUT</p>
                <p>SERVICES</p>
                <p>BLOG</p>
                <p>CARRERS SYSTEM</p>
                </div>
            </div>
            <p style={{ color: 'gray', fontSize: '12px' }}>We pride ourselves on a well defined, tried & tested, no-bull approach to designing award winning products & brands.
                Our designers have their teeth cut in digital design, you are the specialist in your field, together, we create products that disrupt and brands that captivate.</p>
                </div>
                </div>
        </div>
    )
}
