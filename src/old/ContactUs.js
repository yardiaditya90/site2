import React, { useState } from 'react'
import '../styles/MainPage.css'
import { Fade } from "react-awesome-reveal";
import axios from 'axios';
export default function ContactUs() {
    const [css, changeCSS] = useState('')
    const [name, changeName] = useState('');
    const [job, changeJob] = useState('');
    const [company, changeCompany] = useState('');
    const [website, changeWebsite] = useState('');
    const [email, changeEmail] = useState('');
    const [phone, changePhone] = useState('');
    const [briefDescription, changeBriefDescription] = useState('');
    const [option, changeOption] = useState('1');

    const arrow = '→'
    const handleSelect = (e) => {
        e.target.parentNode.parentNode.className = 'element-selected'
    }

    const handleBlur = (e) => {
        e.target.parentNode.parentNode.className = 'element'
    }
    const handleSubmit=async()=>{
        const requestData={
            'option':option,
            'name':name,
            'job':job,
            'company':company,
            'website':website,
            'email':email,
            'phone':phone,
            'briefDescription':briefDescription,
            'option':option
        }
        try{
            const response=await axios.post('http://localhost:8000/api/contactUs',requestData)
            if(response.data.status==true){
                alert(response.data.message)
            }
            else{
                alert("Something else also went wrong")
            }
        }
        catch(e){
            alert('Something went wrong')
        }
        
    }
    return (
        <div style={{backgroundColor:'black'}}>
            <Fade>
            {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
            <div className='contact-us' style={{ paddingBottom: '29px' ,margin:'auto'}}>
                <div className='top'>
                    <h1>Contact</h1>
                </div>
                <div>
                    <p style={{ marginTop: '0px', marginBottom: '5px',marginLeft:'20px' }} className='heading-text'>How can we help?</p>
                </div>

                <div className='contact-box' style={{ marginLeft: '15px', marginTop: '12px' }}>
                    <button className={`contact-btn${(option == '1' ? '-selected' : '')}`} onClick={() => (changeOption('1'))}>I NEED PRODUCT</button>
                    <button className={`contact-btn${(option == '2' ? '-selected' : '')}`} onClick={() => (changeOption('2'))}>I WANT TO WORK HERE</button>
                    <button className={`contact-btn${(option == '3' ? '-selected' : '')}`} onClick={() => (changeOption('3'))}>SOMETHING ELSE</button>
                </div>


                <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <div className='hidden-text' style={{ maxWidth: '50%' }}>
                        <p style={{fontSize:'25px'}}>seoiofnhesoifnsoedi jknv vnvnid vod njvlndvjdjlnlid  kjbk vkjbv fsdnvoisd vlsld visdmmvod jmvodmvosdm vodmvodmvo dvo ksdlvjsdoiv jsdpov jpv dpv.</p>
                    </div>
                    
                    <div className='hidden-text-visible-stuff' style={{paddingRight:'10px'}}>
                        <div className='form'>
                            <div className='element'>
                                <div className='form-input'>
                                    <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={name} onChange={(e) => (changeName(e.target.value))} />
                                    {/* changeCSS('-selected') */}
                                    <label>NAME</label> </div>
                                <div className='divide-bar'></div>

                            </div>
                            <div className='element'>
                                <div className='form-input'>
                                    <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={job} onChange={(e) => (changeJob(e.target.value))} />
                                    <label>JOB ROLE</label>
                                </div>
                                <div className='divide-bar'></div>

                            </div>
                            <div className='element'>
                                <div className='form-input'>
                                    <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={company} onChange={(e) => (changeCompany(e.target.value))} />
                                    <label>COMPANY</label>
                                </div>
                                <div className='divide-bar'></div>

                            </div>
                            <div className='element'>
                                <div className='form-input'>
                                    <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={website} onChange={(e) => (changeWebsite(e.target.value))} />
                                    <label>WEBSITE</label>
                                </div>
                                <div className='divide-bar'></div>

                            </div>
                            <div className='element'>
                                <div className='form-input'>
                                    <input type='email' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={email} onChange={(e) => (changeEmail(e.target.value))} />
                                    <label>EMAIL</label>
                                </div>
                                <div className='divide-bar'></div>

                            </div>
                            <div className='element'>
                                <div className='form-input'>
                                    <input type='number' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={phone} onChange={(e) => (changePhone(e.target.value))} />
                                    <label>PHONE</label>
                                </div>
                                <div className='divide-bar'></div>
                            </div>

                            <div className='element'>
                                <div className='form-input'>
                                    <input type='text' onBlur={(e) => (handleBlur(e))} onClick={(e) => (handleSelect(e))} value={briefDescription} onChange={(e) => (changeBriefDescription(e.target.value))} />
                                    <label>BRIEF DESCRIPTION</label>
                                </div>
                                <div className='divide-bar'></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div style={{ display: 'flex', padding: '8px', paddingTop: '25px', paddingBottom: '25px', margin: 'auto' }}>
                    <p style={{ fontSize: '12px', margin: '2px', color: 'gray' }}>@2023</p>
                    <div style={{ width: '95%', height: '1px', backgroundColor: 'gray', margin: 'auto' }}></div>
                </div>
                <div>
                    <p style={{ fontSize: '20px', marginTop: '0px', marginBottom: '5px' }}>A team of 200 people, it's really cool</p>
                </div>
                <div style={{ width: '95%', height: '.5px', backgroundColor: 'gray', margin: 'auto', marginTop: '30px', marginBottom: '20px' }}></div>
                <div style={{ paddingTop: '20px', display: 'flex', paddingInline: '15px', justifyContent: 'space-between' }}>
                    <button className='btn-white-big' onClick={handleSubmit}>SEND {arrow}</button>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <p style={{ display: 'flex', color: 'gray', fontSize: '12px', margin: 'auto', marginBottom: '5px' }}>OR EMAIL US</p>
                        <button style={{ backgroundColor: 'rgb(0,0,0,0)', color: 'gray', border: '1px solid gray', padding: '6px', fontSize: '10x', margin: 'auto' }}>HEY@gmail.com {arrow}</button>
                    </div>
                </div>
            </div>
            <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }} className='gradient'></div>
            </Fade>
        </div>
    )
}
