import React from 'react'
import '../styles/MainPage.css'
import { Fade } from 'react-awesome-reveal'
export default function ProductPage() {
  return (<>
    {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div> */}
    <div style={{ backgroundColor: 'black', padding: '20px' ,width:'fit-content',paddingRight:'20px'}}>
      <div className='header-responsive'>
        <Fade>

      
      <div>
        <h1 style={{ color: 'white', textAlign: 'left' }} className='heading-text'>Product Name</h1>
        <h2 style={{ color: 'gray', textAlign: 'left' }} className='heading-text'>America's 1 journaling program to help bring your goals to life.</h2>
      </div>

      <div>
        <div style={{ display: 'flex', flexDirection: 'row' }}>


          <p style={{ color: 'gray', display: 'flex' }}>TLDR : </p>
          <div style={{ display: 'flex', width: '70%', height: '1px', backgroundColor: 'gray', margin: 'auto', marginLeft: '3px' }}></div>
        </div>
        <p style={{ color: 'gray', textAlign: 'left' }}>This is an verified product
        </p>

        <div className='button-box'>
          <button>BRAND</button>
          <button>PRODUCT DESIGN</button>
          <button>PROTOTYPE</button>
        </div>
      </div>
      </div>
      <img src={require('../images/productImage.jpg')} style={{ width: '100%', height: 'auto', borderRadius: '25px' }} className='product-image' />

      <div className='info-section'>
        <h2>About Them</h2>
        <p>
          Based in the bustling metropolis of New York City, Amazing Company isn't just an IT firm—it's a beacon of innovation, a testament to excellence, and a catalyst for change. With a dedicated team of tech aficionados, Amazing Company stands at the forefront of technological advancement, offering groundbreaking solutions and transformative strategies to businesses worldwide. Their passion for innovation fuels their relentless pursuit of excellence, driving them to exceed expectations and redefine industry standards. From pioneering software developments to cutting-edge digital initiatives, Amazing Company is committed to empowering their clients to thrive in the ever-evolving digital landscape. With creativity, expertise, and unwavering dedication, they continue to shape the future of IT, one groundbreaking solution at a time.
        </p>
        
      </div>
      
    </div></>
  )
}
