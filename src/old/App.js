import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import Header from './components/Header';
import Carousel from './components/Carousel';
import ProductsArea from './components/ProductsArea';
import SlidingImages from './components/SlidingImages';
import ProductPage from './components/ProductPage';
import MainPage from './components/MainPage';
import Footer from './components/Footer';
import AboutPage from './components/AboutPage';
import { BrowserRouter ,Route,Routes} from "react-router-dom";
import ContactUs from './components/ContactUs';
import { useEffect,useState } from 'react';
import { Fade } from "react-awesome-reveal";

function App() {
  // const [scrollPosition, setScrollPosition] = useState(0);

  // const handleScroll = () => {
  //   const position = window.pageYOffset;
  //   setScrollPosition(position);
  // };

  // useEffect(() => {
  //   window.addEventListener('scroll', handleScroll);

  //   return () => {
  //     window.removeEventListener('scroll', handleScroll);
  //   };
  // }, []);
  return (
    <div className="App">
      <BrowserRouter>
      <div style={{position:'fixed',top:'0px',width:'100%',zIndex:'50'}}>
      <NavBar/>
      </div> 
      <Fade>    
      
      <NavBar/>
      {/* <PageTransition
              preset="moveToLeftFromRight"
              // transitionKey={location.pathname}
            > */}
      <Routes>
      
        <Route path='/' element={<MainPage/>}/> 
        <Route path='/about' element={<AboutPage/>}/>    
        <Route path='/product' element={<ProductPage/>}/> 
        <Route path='/contactUs' element={<ContactUs/>}/> 
       
      {/* <AboutPage/> */}
      {/* <ProductPage/> */}
      {/* <MainPage/> */}
      {/* <Header/> */}
      {/* <SlidingImages/> */}
      {/* <p>Slider</p> */}
      {/* <ProductsArea/> */}
      
      </Routes>
      {/* </PageTransition> */}
      <Footer/>
      </Fade>
      </BrowserRouter>
    </div>
  );
}

export default App;
