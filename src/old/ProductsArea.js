import React from 'react'
import '../styles/Products.css'
import { useNavigate } from 'react-router-dom'
import { Fade } from "react-awesome-reveal";
export default function ProductsArea() {
    const navigate = useNavigate();
    return (
        <div className='product-display-area'>

            <div>
                <p style={{ color: 'white', textAlign: 'left', margin: '20px', fontWeight: 'bold', fontSize: '20px' }}>
                    Everybody trusts this website cause it's the best ever!!!
                    {/* Everybody gangsta until they use this site!!! */}
                </p>
                <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '1' }}></div>
            </div>

            <div className='products'>
                {/* <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '1' }}></div> */}

                <div className='left'>
                    <div className='left-left'>
                        <Fade cascade top>
                            <div className='product-card'>
                                <img src={require('../images/image1.jpg')} onClick={() => (navigate('/product'))} />
                            </div>

                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>

                            <div className='product-card'>
                                <img src={require('../images/image3.jpg')} />
                            </div>

                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                        </Fade>
                    </div>
                    <div className='left-right'>
                        <Fade cascade top>
                            <div className='product-card'>
                                <img src={require('../images/image1.jpg')} />
                            </div>
                            <div className='product-card'>
                                <img src={require('../images/image3.jpg')} />
                            </div>
                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                        </Fade>
                    </div>
                </div>
                <div className='right'>

                    <div className='right-left'>
                        <div className='product-card'>
                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                            <div className='product-card'>
                                <img src={require('../images/image2.jpg')} />
                            </div>
                            <img src={require('../images/image1.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image3.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image2.jpg')} />
                        </div>
                    </div>
                    <div className='right-right'>
                        <div className='product-card'>
                            <img src={require('../images/image2.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image2.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image2.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image2.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image1.jpg')} />
                        </div>
                        <div className='product-card'>
                            <img src={require('../images/image3.jpg')} />
                        </div>
                    </div>

                </div>

                {/* <div className='product-card'>
                    <img src={require('../images/image1.jpg')} />
                </div>

                <div className='product-card'>
                    <img src={require('../images/image2.jpg')} />
                </div>

                <div className='product-card'>
                    <img src={require('../images/image3.jpg')} />
                </div>

                <div className='product-card'>
                    <img src={require('../images/image2.jpg')} />
                </div>
                <div className='product-card'>
                    <img src={require('../images/image2.jpg')} />
                </div> */}
            </div>

        </div>

    )
}
