import axios from 'axios';
import React, { useState } from 'react'

export default function AddProduct() {
    const [productName,changeProductName]=useState('');
    const [productPrice,changeProductPrice]=useState('');
    const [productCateory,changeProductCategory]=useState('');
    const [profileImage,changeProfileImage]=useState('');
    const [productImage,changeProductImage]=useState('');
    const [productImage2,changeProductImage2]=useState('');
    const [productImage3,changeProductImage3]=useState('');
    const [productImage4,changeProductImage4]=useState('');
    const [productDescription,changeProductDescription]=useState('');
    const [productCompany,changeProductCompany]=useState('');
    const [productHTML,changeProductHTML]=useState('');

    const handleSubmit=async()=>{
        const requestData={
            "name":productName,
            "price":productPrice,
            "category":productCateory,
            "profileImage":profileImage,
            "image":[productImage,productImage2,productImage3,productImage4],
            "description":productCateory,
            "company":productCompany,
            "html":productHTML
        }
        console.log("Request data is : ",productImage)
        const response=await axios.post('http://localhost:8000/api/insertProduct',requestData)
        if(response.data.status==true){
            alert("Product Inserted")
        }
        else{
            alert("Something went wront")
        }
    }
    const fileToBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = () => {
                // const base64String = reader.result.split(',')[1]; // Extract base64 data from the result
                const base64String = reader.result
                resolve(base64String);
            };

            reader.onerror = (error) => {
                reject(error);
            };

            reader.readAsDataURL(file); // Convert the file to base64
        });
    };
    
    const handleImgChange = async (e) => {
        console.log("Image change called")
        const file = e.target.files[0];
        if (file) {
            try {
                const base64String = await fileToBase64(file);
                console.log(base64String); // This is your base64-encoded image
                changeProductImage(base64String);
                // console.log(productImage)
                // ChangeImage(file)
                // You can now use this base64String in your application, such as sending it to the server
            } catch (error) {
                console.error('Error converting file to base64:', error);
            }
        }
    };
    const handleImgChange2 = async (e) => {
        const file = e.target.files[0];
        if (file) {
            try {
                const base64String = await fileToBase64(file);
                // setProductImage(base64String);
            } catch (error) {
                console.error('Error converting file to base64:', error);
            }
        }
    };
  return (
    <div>
      <div style={{display:'flex',flexDirection:'column'}}>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>Product Name:</label>
                <input type='text' value={productName} onChange={(e)=>changeProductName(e.target.value)}/>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>Product Price:</label>
                <input type='number' value={productPrice} onChange={(e)=>changeProductPrice(e.target.value)} />
            </div>

            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>Product Category:</label>
                <input type='text'  value={productCateory} onChange={(e)=>changeProductCategory(e.target.value)}/>
            </div>                         

            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>Product Image:</label>
                <input type='file' onChange={(e)=>handleImgChange(e)} />
            </div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>Product Description:</label>
                <input type='text' value={productDescription} onChange={(e)=>changeProductDescription(e.target.value)} />
            </div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>Product Company:</label>
                <input type='text' value={productCompany} onChange={(e)=>changeProductCompany(e.target.value)} />
            </div>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <label>HTML:</label>
                <input type='text' value={productHTML} onChange={(e)=>changeProductHTML(e.target.value)} />
            </div>
        </div>
        <button style={{backgroundColor:'green'}} onClick={handleSubmit}>Submit</button>
    </div>
  )
}
