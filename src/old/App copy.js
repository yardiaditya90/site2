import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import Header from './components/Header';
import Carousel from './components/Carousel';
import ProductsArea from './components/ProductsArea';
import SlidingImages from './components/SlidingImages';
import ProductPage from './components/ProductPage';
import MainPage from './components/MainPage';
import Footer from './components/Footer';
import AboutPage from './components/AboutPage';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ContactUs from './components/ContactUs';
import { useEffect, useState } from 'react';
import { Fade } from "react-awesome-reveal";
import { Routes } from 'react-router-dom';
function App() {
  // ... (commented out unused code)

  return (
    <div className="App">
      <BrowserRouter>
        <div style={{ position: 'fixed', top: '0px', width: '100%',height:'100%', zIndex: '50' }}>
          <NavBar />
        </div>
        <Fade>
          <NavBar /> {/* Removed duplicate NavBar */}
          <Routes>
            <Route path="/" exact element={<MainPage />} /> {/* Use exact for root path */}
            <Route path="/about" element={<AboutPage />} />
            <Route path="/product" element={<ProductPage />} />
            <Route path="/contactUs" element={<ContactUs />} />
          </Routes>
          <Footer />
        </Fade>
      </BrowserRouter>
    </div>
  );
}

export default App;
