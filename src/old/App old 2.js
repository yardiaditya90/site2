import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import Header from './components/Header';
import Carousel from './components/Carousel';
import ProductsArea from './components/ProductsArea';
import SlidingImages from './components/SlidingImages';
import ProductPage from './components/ProductPage';
import ProductPage2 from './components/ProductPages/ProductPage2';
import ProductPage3 from './components/ProductPages/ProductPage3';
import ProductPage4 from './components/ProductPages/ProductPage4';
import ProductPage5 from './components/ProductPages/ProductPage5';
import ProductPage6 from './components/ProductPages/ProductPage6';
import ProductPage7 from './components/ProductPages/ProductPage7';
import ProductPage8 from './components/ProductPages/ProductPage8';
import MainPage from './components/MainPage';
import Footer from './components/Footer';
import AboutPage from './components/AboutPage';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ContactUs from './components/ContactUs';
import { useEffect, useState } from 'react';
import { Fade } from "react-awesome-reveal";
import NewNavBar from './components/NewNavbar';
import AddProduct from './components/AddProduct';
import ProductPage9 from './components/ProductPages/ProductPage9';
import ProductPage10 from './components/ProductPages/ProductPage10';
import ProductPage11 from './components/ProductPages/ProductPage11';
import ProductPage12 from './components/ProductPages/ProductPage12';
function App() {


  // useEffect(() => {
  //   window.addEventListener('scroll', handleScroll);

  //   return () => {
  //     window.removeEventListener('scroll', handleScroll);
  //   };
  // }, []);
  return (
    <div className="App">
      <BrowserRouter>
        {/* <div style={{ position: 'fixed', top: '0px', width: '100%', zIndex: '55',willChange:'width' }}>
          <NewNavBar />
        </div> */}
        <div style={{ position: 'fixed', top: '0px', width: '100%', zIndex: '40' }}>
          <NavBar />
        </div>
        <Fade>

          <NavBar />
          {/* <PageTransition
              preset="moveToLeftFromRight"
              // transitionKey={location.pathname}
            > */}
          <Routes>
            <Route path='/' element={<MainPage />} />
            <Route path='/about' element={<AboutPage />} />
            <Route path='/product' element={<ProductPage />} />
            <Route path='/contactUs' element={<ContactUs />} />
            <Route path='/addProduct' element={<AddProduct />} />


            <Route path='/product2' element={<ProductPage2 />} />
            <Route path='/product3' element={<ProductPage3 />} />
            <Route path='/product4' element={<ProductPage4 />} />
            <Route path='/product5' element={<ProductPage5 />} />
            <Route path='/product6' element={<ProductPage6 />} />
            <Route path='/product7' element={<ProductPage7 />} />
            <Route path='/product8' element={<ProductPage8 />} />
            <Route path='/product9' element={<ProductPage9 />} />
            <Route path='/product10' element={<ProductPage10 />} />
            <Route path='/product11' element={<ProductPage11 />} />
            <Route path='/product12' element={<ProductPage12 />} />
            {/* <AboutPage/> */}
            {/* <ProductPage/> */}
            {/* <MainPage/> */}
            {/* <Header/> */}
            {/* <SlidingImages/> */}
            {/* <p>Slider</p> */}
            {/* <ProductsArea/> */}

          </Routes>
          {/* </PageTransition> */}
          <Footer />
        </Fade>
      </BrowserRouter>
    </div>
  );
}

export default App;