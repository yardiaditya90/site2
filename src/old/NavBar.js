import React, { useState } from 'react'
import '../styles/MainPage.css'
import { Link, useNavigate } from 'react-router-dom'

export default function NavBar() {
    const navigate = useNavigate();
    const arrow = `→`;
    const [dropDown, changeDropDown] = useState(null);

    const handleDropDown = () => {
        if (dropDown === null) {
            changeDropDown(
                <div className='dropdown-list'>
                    <div className='dropdown-list-items'>
                        <Link className='dropdown-link' onClick={closeDropDown} to='/'>Home</Link> <h2>{arrow}</h2>
                    </div>
                    <div className='dropdown-list-items'>
                        <h2>Work</h2> <h2>{arrow}</h2>
                    </div>
                    <div className='dropdown-list-items'>
                        <h2>Services</h2> <h2>{arrow}</h2>
                    </div>
                    <div className='dropdown-list-items'>
                        <Link className='dropdown-link' onClick={closeDropDown} to='/about'>About</Link> <h2>{arrow}</h2>
                    </div>
                    <div className='dropdown-list-items'>
                        <h2>Blog</h2> <h2>{arrow}</h2>
                    </div>
                    <div className='dropdown-list-items'>
                        <h2>Careers</h2> <h2>{arrow}</h2>
                    </div>
                    <div className='dropdown-list-items'>
                        <Link className='dropdown-link' onClick={closeDropDown} to='/contactUs'>Contact</Link> <h2>{arrow}</h2>
                    </div>
                    <div><h1>Featured Products</h1></div>
                </div>
            );
        } else {
            changeDropDown(null);
        }
    };

    const closeDropDown = () => {
        changeDropDown(null);
    };

    return (
        <>
            <div className='navBar'>
                <div className='logo' style={{ cursor: 'pointer' }} onClick={() => navigate('/')}>
                    <h1>B</h1>
                </div>
                <div className='dropdown'>
                    <div className='dropDownButton' onClick={handleDropDown}>
                        <div style={{ display: 'none' }}></div>
                        <div className='stripes'></div>
                        <div className='stripes'></div>
                        <div className='stripes'></div>
                    </div>
                </div>
            </div>
            <div style={{ width: '100%', height: '1px', backgroundColor: 'white', zIndex: '-1' }}></div>
            {dropDown}
        </>
    );
}
