import React, { useEffect } from 'react';
import './App.css';
import NavBar from './components/NavBar';
import Footer from './components/Footer';
import { BrowserRouter, Navigate, Route, Routes, useLocation, useNavigate } from "react-router-dom";
import { Fade } from "react-awesome-reveal";
import ProductPage2 from './components/ProductPages/ProductPage2';
import ProductPage3 from './components/ProductPages/ProductPage3';
import ProductPage4 from './components/ProductPages/ProductPage4';
import ProductPage5 from './components/ProductPages/ProductPage5';
import ProductPage6 from './components/ProductPages/ProductPage6';
import ProductPage7 from './components/ProductPages/ProductPage7';
import ProductPage8 from './components/ProductPages/ProductPage8';
import MainPage from './components/MainPage';
import AboutPage from './components/AboutPage';
import ContactUs from './components/ContactUs';
import AddProduct from './components/AddProduct';
import ProductPage9 from './components/ProductPages/ProductPage9';
import ProductPage10 from './components/ProductPages/ProductPage10';
import ProductPage11 from './components/ProductPages/ProductPage11';
import ProductPage12 from './components/ProductPages/ProductPage12';
import { useState } from 'react';
// import PageTransition from './components/PageTransition';
// var mode='close'
function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);
  return null;
}
function Transition() {
  const { pathname } = useLocation();
  const [mode, setMode] = useState('open');
  useEffect(() => {
    // window.scrollTo(0, 0);
    setMode('open')
    setTimeout(()=>{
      setMode('close')
    },1000)
    
    console.log("New page opened")
  }, [pathname]);
  return (<>
  <div style={{ backgroundColor: 'black', zIndex: '50', position: 'fixed' }} className={mode}></div>
  </>);
}
function App() {
  const [prevPathname, setPrevPathname] = useState(null);
  // const { pathname } = useLocation();
  const [mode, setMode] = useState('close');

  useEffect(() => {
    setMode('open');
    setTimeout(() => {
      console.log("Navigation called");
      setMode('close');
    }, 1000);
  }, []);
  return (
    <div className="App" style={{ backgroundColor: 'black', maxWidth: '100%', width: '100%' }}>

      <BrowserRouter>
        {/* <div style={{ backgroundColor: 'green', zIndex: '50', position: 'fixed' }} className={mode}></div> */}
        <Transition/>
        {/* <PageTransition/> */}
        {/* <PageTransition/> */}
        <div style={{ position: 'fixed', top: '0px', width: '100%', zIndex: '40' }}>
          <NavBar />
        </div>
        <NavBar />
        <Fade>
          <Routes>
            <Route path='/' element={<MainPage />} />
            <Route path='/about' element={<AboutPage />} />
            <Route path='/contactUs' element={<ContactUs />} />
            <Route path='/addProduct' element={<AddProduct />} />
            <Route path='/product2' element={<ProductPage2 />} />
            <Route path='/product3' element={<ProductPage3 />} />
            <Route path='/product4' element={<ProductPage4 />} />
            <Route path='/product5' element={<ProductPage5 />} />
            <Route path='/product6' element={<ProductPage6 />} />
            <Route path='/product7' element={<ProductPage7 />} />
            <Route path='/product8' element={<ProductPage8 />} />
            <Route path='/product9' element={<ProductPage9 />} />
            <Route path='/product10' element={<ProductPage10 />} />
            <Route path='/product11' element={<ProductPage11 />} />
            <Route path='/product12' element={<ProductPage12 />} />
          </Routes>
          <Footer />
        </Fade>
        <ScrollToTop />
        
      </BrowserRouter>
    </div>
  );
}

export default App;
